# Interactive Semantic Mapper

This is a kernel from our OntoNet toolkit for knowledge modeling. It focuses on visualizing semantic relationships in 2D by:
1. Making deep neural net (BERT) projections of text into high-dimensional vectors. 
2. Then projecting into 2D with UMAP

The BERT projection is done with [sentence-transformers](https://github.com/UKPLab/sentence-transformers) ([a Siamese BERT for sentence embedding - see the original paper by Reimers and Gurevych](https://arxiv.org/abs/1908.10084))

The demo visualization is very primitive, and one could take the matrices produced in the code and create much better UIs. 

# Instructions
1. Data & environment  prep: 
- Edit ontonet_backend.jl to point to your CSVs and set the column names
- Install the Python and Julia packages listed on the 2 files

2. Run the text_embed_API.py - this will start a local REST service for sentence-transformers

3. Run ontonet_backend.jl - it will initialize and load the main function runViz(navQ, topN_num) which you can then call from the Julia prompt with different contexts (navQ) and vary the number of top-N instances visualized. 

# AirTable Extension
There is a discontinued version that works as an AirTable extension (for direct mapping of AirTable records) at: https://github.com/martindkaz/ikn. However, it runs all the algorithms from browser JS so it does not scale or perform well. One could use this old repository to combine it with the new one so that the computation is offloaded through APIs.
