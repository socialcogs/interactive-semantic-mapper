from flask import Flask, request
from sentence_transformers import SentenceTransformer
import json

sentBERT = SentenceTransformer('paraphrase-mpnet-base-v2')
####### MAKE SURE YOU UPDATE WHERE DIMs MANUALLY SET (768 currently)

# sentBERT = SentenceTransformer('distilroberta-base-paraphrase-v1')

app = Flask(__name__)

@app.route("/text_embed", methods=['GET', 'POST'])
def text_embed_API():
    data = request.get_json()
    textEmbs = sentBERT.encode(list(data.values()))
    dataEmb = dict(zip(data.keys(),textEmbs.tolist()))
    # print(request.args)
    
    return json.dumps(dataEmb)

if __name__ == "__main__":
    app.run(debug=True)