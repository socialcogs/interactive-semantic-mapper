using HTTP
using JSON2
using UMAP
using Plots
using CSV
using Distances

id2txt = Dict{String, String}()
id2meta = Dict{String, String}()

csvFile = CSV.File("/home/martindkaz/Dropbox/michelle_academic_matching/real_data.csv")
# Set the CSV column names below:
for row in csvFile
    id2txt["$(row.Name)"] = "$(row.Discipline)" * "; " * "$(row.Expertise_1)" * "; " * "$(row.Expertise_2)" * "; " * "$(row.Expertise_3)" * "; " * "$(row.Interests_1)" * "; " * "$(row.Interests_2)" * "; " * "$(row.Interests_3)"
    id2meta["$(row.Name)"] = "$(row.Home_Institution)" * "; " * "$(row.Role)" * "; " * "$(row.Country)"
end

all_txt = collect(values(id2txt))
all_ids = collect(keys(id2txt))

httpGET = HTTP.request("GET", "http://127.0.0.1:5000/text_embed", ["Content-Type" => "application/json"], JSON2.write(id2txt))
embDataStr = String(httpGET.body);

id2emb = JSON2.read(embDataStr, Dict{String, Vector{Float64}})

# ToDo: obtain auto from emdData dict
embedding_size = 768
# emb_matrix = Array{Float64}(undef,embedding_size,0)
# for (key,embV) in id2emb
#     global emb_matrix = hcat(emb_matrix, embV)
# end
# emb_matrix = hcat([y for (x,y) in id2emb]...)

function print_data(ids)
    for id in ids
        println("###   ", id)
        println("- NLP TXT: ", id2txt[id])
        println("- METADATA: ", id2meta[id])
        println()
    end
end



function topN_in_context(ids, navQ, topN_num)
    httpGET = HTTP.request("GET", "http://127.0.0.1:5000/text_embed", ["Content-Type" => "application/json"], JSON2.write(Dict([("navQ", navQ)])))
    emb_navQ_str = String(httpGET.body);
    emb_navQ = JSON2.read(emb_navQ_str, Dict{String, Vector{Float64}})

    emb_matrix = Array{Float64}(undef,embedding_size,0)
    for id in ids
        emb_matrix = hcat(emb_matrix, id2emb[id])
    end

    navQ_distances = colwise(CosineDist(), emb_matrix, emb_navQ["navQ"])
    # symSize, navQ_distances, & all_ids have the same indexing
    sortOrder = sortperm(navQ_distances)
    topN_indx = sortOrder[1:topN_num]

    topN_dist = navQ_distances[topN_indx]
    topN_ids = all_ids[topN_indx]

    print_data(topN_ids)

    return topN_ids, topN_dist
end



function viz_group_with_txtLbls(ids, txtLbls)
    if length(ids) <4
        print("Cluster needs to have more than 3 points to visualize")
        return
    end
    # UMAP.jl: first dimension is num_features, second is num_instances
    if length(ids) < 11 
        umap_num_neighb = 3
    else
        umap_num_neighb = 10
    end
    emb_matrix = Array{Float64}(undef,embedding_size,0)
    for id in ids
        emb_matrix = hcat(emb_matrix, id2emb[id])
    end
    umap_projection = umap(emb_matrix, 2, n_neighbors=umap_num_neighb, metric=CosineDist())

    print_data(ids)

    scatter(umap_projection[1,:], umap_projection[2,:], series_annotations = txtLbls, dpi=300, size=(600,400))

    # savefig("cluster" * string(cID) * ".png")
end

function viz(ids)
    viz_group_with_txtLbls(ids, ids)
end

function viz(ids, dist)
    min_dist = minimum(dist)
    max_dist = maximum(dist)
    symSize = 7 * (dist .- min_dist)/(max_dist-min_dist) .+ 3
    symSize = Int64.(round.(symSize))

    txtLbls = [text(ids[i], symSize[i]) for i=1:length(ids)]

    viz_group_with_txtLbls(ids, txtLbls)
end

# savefig("image1.png")



