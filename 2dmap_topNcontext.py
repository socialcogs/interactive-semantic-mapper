import requests
import json
import numpy as np
from umap import UMAP
from scipy.spatial.distance import cosine
import pandas as pd
import matplotlib.pyplot as plt
from sentence_transformers import SentenceTransformer

# Global dictionaries
id2txt = {}
id2emb = {}
selected_columns = []

xls_path = r"C:\myData\data.xlsx"

# Load data from CSV
def load_rows(xls_path):
    global id2txt, selected_columns
    df = pd.read_excel(xls_path)
    
    # Print column names
    print("\nChoose the columns to include:")
    for columnID, column in enumerate(df.columns, 1):
        print(f"{columnID}: {column}")
    
    # Get user input for column selection
    column_input = input("\nEnter column numbers (separated by spaces or comma): ")
    selected_columns = [int(col.strip()) - 1 for col in column_input.replace(',', ' ').split()]
    
    # Construct text for each row
    for idx, row in df.iterrows():
        rowID = idx + 1
        # Construct text by joining selected column values with column names
        txt = "; ".join(f"{df.columns[col].replace('_', ' ')}: {str(row[df.columns[col]])}" for col in selected_columns)
        id2txt[rowID] = txt
        print(f"\n{rowID}. {txt}")  # Print the constructed text for verification

def load_columns(xls_path):
    global id2txt, selected_columns
    df = pd.read_excel(xls_path)
    
    # Print column names
    print("\nChoose the columns to include:")
    for columnID, column in enumerate(df.columns, 1):
        print(f"{columnID}: {column}")
    
    # Get user input for column selection
    column_input = input("\nEnter column numbers (separated by spaces or comma): ")
    selected_columns = [int(col.strip()) - 1 for col in column_input.replace(',', ' ').split()]
    
    # Create entries for each cell in selected columns
    for idx, row in df.iterrows():
        rowID = idx + 1
        for col_idx, col in enumerate(selected_columns):
            # Create key in format "rowID.columnID"
            cell_key = f"{rowID}.{col + 1}"  # Adding 1 to match 1-based column indexing
            # Create text with column name and cell value
            cell_txt = f"{df.columns[col].replace('_', ' ')}: {str(row[df.columns[col]])}"
            id2txt[cell_key] = cell_txt
            print(f"\n{cell_key}. {cell_txt}")  # Print for verification

# Get embeddings from server
def get_embeddings_cpu():
    global id2emb

    # Initialize the model
    model = SentenceTransformer('sentence-transformers/all-mpnet-base-v2')
    
    # Get texts in the same order as ids
    texts = [id2txt[id] for id in sorted(id2txt.keys())]
    
    # Generate embeddings
    embeddings = model.encode(texts, show_progress_bar=True)
    
    # Store embeddings in id2emb dictionary
    for idx, id in enumerate(sorted(id2txt.keys())):
        id2emb[id] = embeddings[idx].tolist()

def get_embeddings_cuda():
    global id2emb
    from sentence_transformers import SentenceTransformer
    query_prompt_name = "s2s_query"
    model = SentenceTransformer("dunzhang/stella_en_1.5B_v5", trust_remote_code=True).cuda()
    query_embeddings = model.encode(id2txt.values(), prompt_name=query_prompt_name)
    print(query_embeddings.shape)

def print_data(ids):
    for id in ids:
        print("###   ", id)
        print("- NLP TXT: ", id2txt[id])
        print()

def topN_in_context(ids, navQ, topN_num):
    # Get embedding for navigation query
    response = requests.get(
        "http://127.0.0.1:5000/text_embed",
        headers={"Content-Type": "application/json"},
        data=json.dumps({"navQ": navQ})
    )
    emb_navQ = json.loads(response.text)["navQ"]
    
    # Create embedding matrix
    emb_matrix = np.array([id2emb[id] for id in ids]).T
    
    # Calculate distances
    navQ_distances = [cosine(emb_matrix.T[i], emb_navQ) for i in range(emb_matrix.shape[1])]
    
    # Get top N closest
    topN_indices = np.argsort(navQ_distances)[:topN_num]
    topN_dist = [navQ_distances[i] for i in topN_indices]
    topN_ids = [ids[i] for i in topN_indices]
    
    print_data(topN_ids)
    return topN_ids, topN_dist

umap_num_neighb = 4
umap_model = None
umap_projection = None
emb_matrix = None

def choose_umap_neighbors():
    global umap_num_neighb, umap_projection, umap_model, emb_matrix
    
    while True:
        # Create embedding matrix
        sorted_ids = sorted(id2emb.keys())  # Store sorted IDs for consistent labeling
        emb_matrix = np.array([id2emb[id] for id in sorted_ids])
        
        # UMAP projection
        umap = UMAP(n_neighbors=umap_num_neighb, n_components=2, metric='cosine')
        umap_model = umap.fit(emb_matrix)  # First fit the model
        umap_projection = umap_model.transform(emb_matrix)  # Then transform the data
        
        # Plot
        plt.figure(figsize=(10, 8), dpi=300, num=f'Columns={selected_columns}; UMAP Neighbors={umap_num_neighb}; Plot_{id(plt.gcf())}')
        plt.scatter(umap_projection[:, 0], umap_projection[:, 1])
        
        # Add labels using the actual keys instead of indices
        for i, (x, y) in enumerate(umap_projection):
            plt.annotate(sorted_ids[i], (x, y), fontsize=5)
        
        plt.show(block=False)
        
        # Get user input for next iteration
        user_input = input(f"\nPlotted with UMAP neighbors parameter: {umap_num_neighb}. Try a different number or type'next' to continue: ")
        
        if user_input.lower() == 'next':
            break
        
        try:
            umap_num_neighb = int(user_input)
        except ValueError:
            print("Please enter a valid number or 'next'")
            continue

def plot_with_custom_text():
    regular_points = umap_model.embedding_
    custom_points = []  # Store tuples of (coordinates, label)
    sorted_ids = sorted(id2emb.keys())  # Add this line to get sorted IDs

    while True:
        # Get custom text and label from user
        print("\nEnter custom text/topic.")
        print("Press Ctrl+Z (Windows) + Enter when finished typing:")
        custom_text_lines = []
        try:
            while True:
                line = input()
                custom_text_lines.append(line)
        except (EOFError, KeyboardInterrupt):
            custom_text = '\n'.join(custom_text_lines)
        
        if custom_text.lower() == 'done':
            break
        custom_label = input("Enter a label for this text/topic: ")
        
        # Get embedding for custom text
        model = SentenceTransformer('sentence-transformers/all-mpnet-base-v2')
        custom_embedding = model.encode([custom_text])[0]
        
        # Transform the embedding and store coordinates with label
        transformed_point = umap_model.transform([custom_embedding])[0]
        custom_points.append((transformed_point, custom_label))
        
        # Plot
        plt.figure(figsize=(10, 8), dpi=300, 
                  num=f'Columns={selected_columns}; UMAP Neighbors={umap_num_neighb}; Custom Texts={" & ".join(label for _, label in custom_points)}')
        
        # Plot regular points
        plt.scatter(regular_points[:, 0], regular_points[:, 1], c='blue', alpha=0.6)
        
        # Plot custom points with different colors
        colors = plt.cm.rainbow(np.linspace(0, 1, len(custom_points)))
        for (x, y), color, label in zip(
            [point for point, _ in custom_points],
            colors,
            [label for _, label in custom_points]
        ):
            plt.scatter(x, y, c=[color], s=200, marker='*', label=label)
            plt.annotate(label, (x, y), fontsize=5, fontweight='bold')
        
        # Add regular point labels
        for i, (x, y) in enumerate(regular_points):
            plt.annotate(sorted_ids[i], (x, y), fontsize=5)
        
        plt.show(block=False)

# Ask user for loading preference
load_choice = input("Do you want to load by rows or by column values? (r/c): ").lower()
if load_choice == 'r':
    load_rows(xls_path)
elif load_choice == 'c':
    load_columns(xls_path)
else:
    print("Invalid choice. Defaulting to columns.")
    load_columns(xls_path)

get_embeddings_cpu()
choose_umap_neighbors()
plot_with_custom_text()    


# Usage example:
# get_embeddings()
# 
# ids = list(id2txt.keys())
# viz(ids)
# 
# # Or with context search:
# topN_ids, topN_dist = topN_in_context(ids, "some search query", 10)
# viz(topN_ids, topN_dist)

#  & c:/Users/afleming/interactive-semantic-mapper/.venv/Scripts/python.exe c:/Users/afleming/interactive-semantic-mapper/2dmap_topNcontext.py