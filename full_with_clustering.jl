using HTTP
using JSON2
using UMAP
using Plots
using CSV
using Distances
using Clustering
using Statistics

id2txt = Dict{String, String}()
id2meta = Dict{String, String}()

csvFile = CSV.File("/home/martindkaz/Dropbox/michelle_academic_matching/real_data.csv")
# Set the CSV column names below:
for row in csvFile
    id2txt["$(row.Name)"] = "$(row.Discipline)" * "; " * "$(row.Expertise_1)" * "; " * "$(row.Expertise_2)" * "; " * "$(row.Expertise_3)" * "; " * "$(row.Interests_1)" * "; " * "$(row.Interests_2)" * "; " * "$(row.Interests_3)"
    id2meta["$(row.Name)"] = "$(row.Home_Institution)" * "; " * "$(row.Role)" * "; " * "$(row.Country)"
end

# csvFile = CSV.File("/Users/angie/csv/STAFF_COMPILEDforUBZ.csv")
# # Set the CSV column names below:
# for row in csvFile
#     id2txt["$(row.ID)"] = "$(row.Language_Data)"
#     id2meta["$(row.ID)"] = "$(row.Gender)" * ";" * "$(row.Race)"
# end

all_txt = collect(values(id2txt))
all_ids = collect(keys(id2txt))

httpGET = HTTP.request("GET", "http://127.0.0.1:5000/text_embed", ["Content-Type" => "application/json"], JSON2.write(id2txt))
embDataStr = String(httpGET.body);

id2emb = JSON2.read(embDataStr, Dict{String, Vector{Float64}})

# ToDo: obtain auto from emdData dict
embedding_size = 768
# emb_matrix = Array{Float64}(undef,embedding_size,0)
# for (key,embV) in id2emb
#     global emb_matrix = hcat(emb_matrix, embV)
# end
# emb_matrix = hcat([y for (x,y) in id2emb]...)

function print_data(ids)
    for id in ids
        println("###   ", id)
        println("- NLP TXT: ", id2txt[id])
        println("- METADATA: ", id2meta[id])
        println()
    end
end

function remove_ids(starting_list, to_remove)
    return filter(e->!(e in to_remove), starting_list)
end

function topN_in_context(ids, navQ, topN_num)
    httpGET = HTTP.request("GET", "http://127.0.0.1:5000/text_embed", ["Content-Type" => "application/json"], JSON2.write(Dict([("navQ", navQ)])))
    emb_navQ_str = String(httpGET.body);
    emb_navQ = JSON2.read(emb_navQ_str, Dict{String, Vector{Float64}})

    emb_matrix = Array{Float64}(undef,embedding_size,0)
    for id in ids
        emb_matrix = hcat(emb_matrix, id2emb[id])
    end

    navQ_distances = colwise(CosineDist(), emb_matrix, emb_navQ["navQ"])
    # symSize, navQ_distances, & all_ids have the same indexing
    sortOrder = sortperm(navQ_distances)
    topN_indx = sortOrder[1:topN_num]

    topN_dist = navQ_distances[topN_indx]
    topN_ids = all_ids[topN_indx]

    print_data(topN_ids)

    return topN_ids, topN_dist
end

# Oppose nodes from the same meta1 tag
function adjust_p2pD_oppose_sameMeta(p2p_dists, oppose_addon, id2meta)
    max_in_matrix = findmax(p2p_dists)[1]
    num_data = size(p2p_dists)[1]
    for i in 1 : num_data
        meta_i = id2meta[ids[i]]
        for j in 1 : num_data
            meta_j = id2meta[ids[j]]
            if (meta_i == meta_j) && (i != j) 
                p2p_dists[i,j] = max_in_matrix + oppose_addon
            end
        end
    end
    return p2p_dists
end

function cluster_by_opposing_same_meta1(ids, num_clus, oppose_addon)
    clusters = Dict{Int64, Vector{String}}()
    for i = 1:num_clus
        clusters[i] = Array{String}(undef,0)
    end
    silhouets = Dict{Int64, Array{Float64}}()
    for i = 1:num_clus
        silhouets[i] = Array{Float64}(undef,0)
    end    
    emb_matrix = Array{Float64}(undef,embedding_size,0)
    for id in ids
        emb_matrix = hcat(emb_matrix, id2emb[id])
    end    
    p2p_dists = pairwise(CosineDist(), emb_matrix, dims=2)

    p2p_dists = adjust_p2pD_oppose_sameMeta(p2p_dists, oppose_addon, id2meta)

    R = kmedoids(p2p_dists, num_clus)
    all_silhouet_scores = silhouettes(assignments(R), p2p_dists)
    index = 1
    # ass is the cluter ID
    for ass in R.assignments
        push!(clusters[ass], ids[index])
        push!(silhouets[ass], all_silhouet_scores[index])        
        index = index + 1
    end       

    print_clusters(clusters, silhouets)

    return clusters

end

function iterative_hierarchical(ids, oppose_addon)
    if length(ids) < 5
        print_data(ids)
        return
    end

    emb_matrix = Array{Float64}(undef,embedding_size,0)
    for id in ids
        emb_matrix = hcat(emb_matrix, id2emb[id])
    end    
    p2p_dists = pairwise(CosineDist(), emb_matrix, dims=2)

    p2p_dists = adjust_p2pD_oppose_sameMeta(p2p_dists, oppose_addon, id2meta)

    h = 0.01
    while h < 1 
        R = hclust(p2p_dists)
        cut_R = cutree(R, h = h)

        num_clus = maximum(cut_R)
        clusters = Dict{Int64, Vector{String}}()
        for i = 1:num_clus
            clusters[i] = Array{String}(undef,0)
        end

        clus_size = zeros(Int64,num_clus)

        index = 1
        for cluster_id in cut_R
            push!(clusters[cluster_id], ids[index])
            clus_size[cluster_id] = clus_size[cluster_id] + 1
            if clus_size[cluster_id] > 5
                println(">>>>>>> ERROR: cluster sized jumped over 5")
                return
            elseif clus_size[cluster_id] == 5
                print_data(clusters[cluster_id])
                println("####################")
                filtered_ids = filter(e->!(e in clusters[cluster_id]), ids)
                # print(filtered_ids)
                iterative_hierarchical(filtered_ids, oppose_addon)
                return
            end
            index = index + 1
        end 

        h = h + 0.01
    end
end

# Needs better parametarization, options for adj_matrix, to subsume other clustering functions
function cluster1(ids, num_clus, cType)
    clusters = Dict{Int64, Vector{String}}()
    for i = 1:num_clus
        clusters[i] = Array{String}(undef,0)
    end
    silhouets = Dict{Int64, Array{Float64}}()
    for i = 1:num_clus
        silhouets[i] = Array{Float64}(undef,0)
    end    
    emb_matrix = Array{Float64}(undef,embedding_size,0)
    for id in ids
        emb_matrix = hcat(emb_matrix, id2emb[id])
    end

    if cType == :kMeans
        p2p_dists = pairwise(CosineDist(), emb_matrix, dims=2)
        R = kmeans(emb_matrix, num_clus)
        all_silhouet_scores = silhouettes(assignments(R), p2p_dists)
        index = 1
        for ass in assignments(R)
            push!(clusters[ass], ids[index])
            push!(silhouets[ass], all_silhouet_scores[index])
            index = index + 1
        end
    elseif cType == :kMedoids
        p2p_dists = pairwise(CosineDist(), emb_matrix, dims=2)
        R = kmedoids(p2p_dists, num_clus)
        all_silhouet_scores = silhouettes(assignments(R), p2p_dists)
        index = 1
        for ass in R.assignments
            push!(clusters[ass], ids[index])
            push!(silhouets[ass], all_silhouet_scores[index])
            index = index + 1
        end       
    elseif cType == :Hierarch_K
        p2p_dists = pairwise(CosineDist(), emb_matrix, dims=2)
        R = hclust(p2p_dists)
        index = 1
        for ass in cutree(R, k = num_clus)
            push!(clusters[ass], ids[index])
            index = index + 1
        end          
    elseif cType == :Hierarch_H
        p2p_dists = pairwise(CosineDist(), emb_matrix, dims=2)
        R = hclust(p2p_dists)
        cut_R = cutree(R, h = num_clus)
        num_clus = maximum(cut_R)
        clusters = Dict{Int64, Vector{String}}()
        for i = 1:num_clus
            clusters[i] = Array{String}(undef,0)
        end
        index = 1
        for ass in cut_R
            push!(clusters[ass], ids[index])
            index = index + 1
        end 
    elseif cType == :MCF
        p2p_dists = pairwise(CosineDist(), emb_matrix, dims=2)
        R = mcl(p2p_dists, save_final_matrix=true, display=:iter)
        index = 1
        for ass in R.assignments
            push!(clusters[ass], ids[index])
            index = index + 1
        end 
    end

    print_clusters(clusters, silhouets)

    return clusters
end

function print_clusters(clusters, silhouets)
    num_clus = length(clusters)

    for i = 1:num_clus
        if size(clusters[i])[1] > 1
            println()
            println()
            println("### CLUSTER ", i)
            print_data(clusters[i])
        end
    end   

    println()
    println()
    println("### CLUSTER SIZES: ")
    for i = 1:num_clus
        println("Cluster ", i, ": ", size(clusters[i])[1])        
    end

    println()
    println()
    println("### SILHOUETTES SCORES: ")
    for i = 1:num_clus
        println("Cluster ", i, ": ", mean(silhouets[i]))        
    end
    println()
end


function viz_clusters()

end



function viz_group_with_txtLbls(ids, txtLbls)
    if length(ids) <4
        print("Cluster needs to have more than 3 points to visualize")
        return
    end
    # UMAP.jl: first dimension is num_features, second is num_instances
    if length(ids) < 11 
        umap_num_neighb = 3
    else
        umap_num_neighb = 10
    end
    emb_matrix = Array{Float64}(undef,embedding_size,0)
    for id in ids
        emb_matrix = hcat(emb_matrix, id2emb[id])
    end
    umap_projection = umap(emb_matrix, 2, n_neighbors=umap_num_neighb, metric=CosineDist())

    print_data(ids)

    scatter(umap_projection[1,:], umap_projection[2,:], series_annotations = txtLbls, dpi=300, size=(600,400))

    # savefig("cluster" * string(cID) * ".png")
end

function viz(ids)
    viz_group_with_txtLbls(ids, ids)
end

function viz(ids, dist)
    min_dist = minimum(dist)
    max_dist = maximum(dist)
    symSize = 7 * (dist .- min_dist)/(max_dist-min_dist) .+ 3
    symSize = Int64.(round.(symSize))

    txtLbls = [text(ids[i], symSize[i]) for i=1:length(ids)]

    viz_group_with_txtLbls(ids, txtLbls)
end

# savefig("image1.png")



